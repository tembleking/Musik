const express = require('express');
const session = require("express-session");
const hbs = require('hbs');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const flash = require('connect-flash');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const index = require('./routes/index');
const login = require('./routes/login');
const explore = require('./routes/explore');
const about = require('./routes/about');
const detail = require('./routes/detail');
const pricing = require('./routes/pricing');
const privacy = require('./routes/privacy');
const settings = require('./routes/settings');

// Registra todos los posibles eventos que se pueden dar
require('./etc/events');


const mongo = require('mongodb');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const config = require('./etc/config');
const expressValidator = require('express-validator');

const dbURI = "mongodb://" + config.db.username + ":" +
    config.db.passwd + "@" +
    config.db.host + "/" +
    config.db.database;

console.log(dbURI);
mongoose.connect(dbURI, {useMongoClient: true});
// mongoose.Promise = global.Promise;
mongoose.connection.on('connected', function (ref) {
    console.log('Connected to mongo server.');
});

const app = express();

// view engine setup
hbs.registerPartials(__dirname + '/views/partials');

// Añade a hbs la capacidad de realizar un bucle del tipo for(0..n)
hbs.registerHelper('times', function(n, opts) {
    let accum = '';
    for(let i = 0; i < n; ++i)
        accum += opts.fn(i);
    return accum;
});

// Añade a hbs la capacidad de realizar comparaciones
hbs.registerHelper('if_eq', function(a, b, opts) {
    if(a == b) // Or === depending on your needs
        return opts.fn(this);
    else
        return opts.inverse(this);
});

// Añade a hbs la capacidad de realizar operaciones matematicas
hbs.registerHelper("math", function(lvalue, operator, rvalue) {
    lvalue = parseFloat(lvalue);
    rvalue = parseFloat(rvalue);

    return {
        "+": lvalue + rvalue,
        "-": lvalue - rvalue,
        "*": lvalue * rvalue,
        "/": lvalue / rvalue,
        "%": lvalue % rvalue
    }[operator];

});

hbs.registerHelper("math", function(lvalue, operator, rvalue, opts) {
    lvalue = parseFloat(lvalue);
    rvalue = parseFloat(rvalue);

    return {
        "+": lvalue + rvalue,
        "-": lvalue - rvalue,
        "*": lvalue * rvalue,
        "/": lvalue / rvalue,
        "%": lvalue % rvalue
    }[operator];
});
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/* Express Validator */
app.use(expressValidator({
    errorFormatter: function(param, msg, value) {
        const namespace = param.split('.')
            , root = namespace.shift()
        ;let formParam = root;

        while(namespace.length) {
            formParam += '[' + namespace.shift() + ']';
        }
        return {
            param : formParam,
            msg   : msg,
            value : value
        };
    }
}));


/* EXPRESS SESSION */
app.use(session({
    secret:'secret',
    saveUninitialized: false, // don't create session until something stored
    resave: false, //don't save session if unmodified
    cookie:{
        expires: false,
    },
    rolling: true
}));

/* PASSPORT INIT */
app.use(passport.initialize());
app.use(passport.session());

/* CONNECT FLASH*/
app.use(flash());

/* GLOBAL VARS / SESSION UPDATE */
app.use(function(req, res, next){
    res.locals.user = req.user || null;
    res.locals.success = req.flash("success");
    res.locals.error = req.flash("error");

    //Session update
    req.session.touch();
    // console.log(req.flash("user") || "NOPE");
    next();
});


// Routes
app.use('/', index);
app.use('/login', login);
app.use('/about', about);
app.use('/pricing', pricing);
app.use('/privacy', privacy);
app.use('/explore', explore);
app.use('/detail', detail);
app.use('/settings', settings);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
