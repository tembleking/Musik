MongoDB
=======

Instalación
-----------
Para instalar el gestor de bases de datos MongoDB, se hace de distintas 
formas dependiendo del entorno:
* En Windows se ha de descargar el paquete de la página oficial.
* En entornos GNU/Linux, se ha de instalar el paquete `mongodb` mediante 
el gestor de paquetes correspondiente a la distribución.
* En entornos MacOS, se puede instalar mediante la página oficial, o 
desde el gestor de paquetes Homebrew.

Configuración inicial
---------------------
En la instalación inicial, el servidor permite conexiones diréctamente, 
sin autentificación. Para restringir el acceso, se ha de crear un 
usuario administrador y relanzar el servidor en modo autentificado.

### Conexión con la base de datos
Podemos utilizar el programa `mongo` para conectar con la base de datos 
y administrarla.
Es tan simple como ejecutar: 

```
$ mongo <url>/<db>
```

Por ejemplo:

```
$ mongo localhost/localDb
```

```
$ mongo remoteServer.com/appDb
```

### Creación del administrador
Para crear el usuario se deben realizar los siguientes pasos:
- Cambiamos de base de datos para utilizar la de `admin`:

```
> use admin
```

- Creamos el usuario, indicandole una contraseña y el rol de 
administrador de todas las bases de datos.

```js
> db.createUser({ user: "<username>",
  pwd: "<password>",
  roles: [
    { role: "userAdminAnyDatabase", db: "admin" }
  ]
})
```

### Restricción de acceso anónimo
- Configuramos el servidor modificando el archivo `/etc/mongodb.conf` y 
agregando la linea

```
auth = true
```

- Reiniciamos el servidor para aplicar la configuración.

A partir de ahora, MongoDB permitirá la conexión a usuarios anónimos, 
pero no les permitirá leer ni modificarla base de datos, a no ser que se 
autentifiquen mediante: 

```js
> db.auth( "<username>", "<password>" )
```

O directamente en la conexión inicial:

```
mongo <url>/<db> -u <username> -p <password>
```

**Importante:** Los usuarios se crean en cada base de datos, no en el 
propio servidor. Si se desea conectar con una base de datos en concreto, 
con un usuario, esa base de datos debe tener el usuario creado y 
configurado para su conexión.

### Creación de usuario
Creamos un usuario directamente en la base de datos a la que vamos a 
conectar y ejecutamos:

```js
> use localDb
> db.createUser({ user: "<username>",
  pwd: "<password>",
  roles: [
    { role: "readWrite", db: "localDb" }
  ]
})
```

A partir de entonces, el usuario puede conectar a la base de datos en 
modo lectura y escritura.

### Creación de colecciones
Las *tablas* en MongoDB son colecciones de datos.
La forma de crear colecciones es mediante el uso de:

```
db.createCollection("<name>", {<options>})
```

**Nota**: No es necesario crear las colecciones al utilizar un ORM como `mongoose`, se 
crean de forma automática al definir los modelos.

Las opciones de la colección no son obligatorias, pero recomendables en caso 
de que se quiera tener consistencia en los datos.
Se puede especificar un validador, un nivel de validación y la fuerza de 
validación mediante las opciones:

* `validator`: Objeto que representa la forma que tienen los objetos 
dentro de la colección y los tipos de sus atributos.
* `validationLevel`: `off`(apagado), `strict`(**por defecto**, aplica 
validación a todos los datos) y `moderate` (aplica validación en 
actualización únicamente en objetos existentes válidos y en la inserción 
de nuevos objetos)
* `validationAction`: `error` (**por defecto**, los objetos deben pasar 
la validación o la escritura falla) o `warn` (la escritura de los 
objetos se lleva a cabo, pero se registra el fallo en la validación)

Existen más opciones al crear la colección pero nosotros no vamos a 
utilizar más.
