var config = {};

config.server = {};
config.db = {};

config.server.name = 'Musik';
config.server.port = 8888;


config.db.host = 'localhost';
config.db.database = 'musik';
config.db.username = 'musik';
config.db.passwd = 'musik';

module.exports = config;
