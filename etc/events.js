const EventEmitter = require("../lib/EventEmitter");
const User = require("../model/User");
const Album = require("../model/Album");

EventEmitter.on("USER_VOTED", (userId, albumId, songName, rating) => {
    User.findById(userId).then(user => {
        Album.findById(albumId).then(album => {
            user.addAction("Votacion", 'Has votado la canción "' + songName + '" del álbum "' + album.name + '" con ' + rating + " estrellas.")
                .save();
        }).catch((error)=>{
            console.error(error);
        });
    }).catch((error)=>{
        console.error(error);
    });
});
EventEmitter.on("USER_LOGGED_IN", (userId) => {
    User.findById(userId).then(user => {
        user.addAction("Inicio de sesión", "Has iniciado sesión en la web.")
            .save();
    }).catch((error)=>{
        console.error(error);
    });
});
EventEmitter.on("USER_SUBSCRIBED", (userId, subscriptionName) => {
    // TODO Agregar subscripciones
});

EventEmitter.on("USER_COMMENT_NEW", (userId, albumId) => {
    User.findById(userId).then(user => {
        Album.findById(albumId).then(album => {
            user.addAction("Comentario", 'Has realizado un comentario en el álbum "' + album.name + '".')
                .save();
        }).catch((error)=>{
            console.error(error);
        });
    });

});
EventEmitter.on("USER_COMMENT_RESPONSE", (userId, albumId, commentId) => {
    User.findById(userId).then(user => {
        Album.findById(albumId).then(album => {
            const comment = album.getComment(commentId);
            User.findById(comment.user).then(userResponse => {
                user.addAction("Respuesta",
                    'Has realizado un comentario de respuesta en el álbum "' + album.name + '" al usuario "' + userResponse.name + '".')
                    .save();
            }).catch((error)=>{
                console.error(error);
            });
        }).catch((error)=>{
            console.error(error);
        });
    }).catch((error)=>{
        console.error(error);
    });

});