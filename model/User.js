const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const Schema = mongoose.Schema;

const PremiumRateSchema = Schema({
    name: String,
    price: Number,
    enabled: Boolean
});

const UserSubscriptionSchema = Schema({
    initial_date: {type: Date, default: Date.now()},
    expiration_date: Date,
    rate: PremiumRateSchema
});

const UserActionSchema = Schema({
    name: String,
    description: String,
    date: {type: Date, default: Date.now()}
});

const UserSchema = Schema({
    password: String,
    email: {type: String, unique: true},
    name: String,
    surname: String,
    birth_date: Date,
    profile_picture: String,
    register_date: {type: Date, default: Date.now()},
    subscriptions: [UserSubscriptionSchema],
    actions: [UserActionSchema],
});

/**
 * Creates new User and returns it
 * @param email
 * @param password
 * @param name
 * @param surname
 * @param birth_date
 * @returns {UserSchema}
 */
UserSchema.statics.create = function (email, password, name, surname, birth_date) {
    return new this({
        "password": bcrypt.hashSync(password, bcrypt.genSaltSync(10)),
        "email": email,
        "name": name,
        "surname": surname,
        "birth_date": birth_date,
    });
};

UserSchema.statics.findByEmail = function(email) {
    return this.findOne({"email": email});
};

/**
 * Change's the user password asyncronously
 * @param newPassword
 * @returns {Promise}
 */
UserSchema.methods.changePassword = function (newPassword) {
    return new Promise(resolve => {
        this.password = bcrypt.hashSync(newPassword, bcrypt.genSaltSync(10));
        resolve(this);
    });
};

/**
 * Check's if the password matches with the user's
 * @param password
 * @returns {Promise}
 */
UserSchema.methods.validatePassword = function (password) {
    return new Promise(resolve => {
        let res = bcrypt.compareSync(password, this.password);
        resolve(res);
    });
};

/**
 * Gets an active subscription from the user
 * @returns {Query|Promise|*}
 */
UserSchema.methods.getActiveSubscription = function () {
    let activeSubscriptions = this.subscriptions.filter(subscription => {
        let now = Date.now();
        return subscription.initial_date <= now && subscription.expiration_date >= now;
    });
    return activeSubscriptions.length > 0 ? activeSubscriptions[0] : null;
};

/**
 * Adds an action to the user
 * @param name          Name of the action
 * @param description   Description of the action
 * @returns {User}
 */
UserSchema.methods.addAction = function (name, description) {
    this.actions.push({
        name: name,
        description: description,
        date: Date.now()
    });
    return this;
};

/**
 * Checks if user has an active subscription.
 * @returns {Promise}
 */
UserSchema.methods.isSubscribed = function () {
    return new Promise((resolve, reject) => {
        this.getActiveSubscription()
            .then(result => {
                if (result.length > 0) {
                    resolve();
                } else {
                    reject();
                }
            });
    });
};


module.exports = mongoose.model("User", UserSchema);
