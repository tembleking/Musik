const mongoose = require('mongoose');
const User = require("./User");
const Schema = mongoose.Schema;

const SongRatingSchema = mongoose.Schema({
    rating: {type: Number, max: 5, min: 0},
    rating_date: {type: Date, default: Date.now},
    user: {type: Schema.Types.ObjectId, ref: 'User'},
});

const SongSchema = mongoose.Schema({
    name: String,
    lyrics: String,
    duration: Number, // Seconds
    ratings: [SongRatingSchema]
});

const AlbumCommentSchema = new Schema();
AlbumCommentSchema.add({
    title: String,
    body: String,
    publish_date: {type: Date, default: Date.now()},
    user: {type: Schema.Types.ObjectId, ref: 'User'},
    comments: [AlbumCommentSchema]
});

const AlbumSchema = mongoose.Schema({
    name: {type: String, unique: true},
    description: String,
    release_date: Date,
    enabled: {type: Boolean, default: true},
    image: String,
    genreName: String,
    authorName: String,

    songs: [SongSchema],
    comments: [AlbumCommentSchema]
});


/////////////// STATICS ///////////////
AlbumSchema.statics.create = function (name, description, release_date, genreName, authorName) {
    return new this({
        name: name,
        description: description,
        release_date: release_date,
        genreName: genreName,
        authorName: authorName
    });
};
AlbumSchema.statics.findByName = function (name) {
    return this.findOne({name: name});
};
AlbumSchema.statics.findByGenre = function (genreName) {
    return this.find({genreName: genreName});
};
AlbumSchema.statics.findByAuthor = function (authorName) {
    return this.find({authorName: authorName})
};
/**
 * Gets a list of all distinct genres
 * @returns {Query|Promise|*}
 */
AlbumSchema.statics.getGenres = function () {
    return this.distinct("genreName");
};

/////////////// METHODS ///////////////
/**
 * Gets the rating mean of all the ratings the song has
 * @returns {number}
 */
SongSchema.methods.getRatingMean = function () {
    return (this.ratings.length > 0) ? (this.ratings.reduce((sumRating, rating) => sumRating + rating.rating, 0) / this.ratings.length) : 0;
};

SongSchema.methods.getDurationString = function () {
    let date = new Date(null);
    date.setSeconds(this.duration);
    let isoDate = date.toISOString();
    return this.duration >= 3600 ? isoDate.substr(11, 8) : isoDate.substr(14, 5);
};

/**
 * Adds or updates a rating to a song from a given user
 * @param userId User ID that rates the song
 * @param rate Rating number
 * @returns {SongSchema.methods}
 */
SongSchema.methods.addOrUpdateRating = function (userId, rate) {
    let userHasVoted = false;
    for(let rating of this.ratings) {
        if (rating.user.toString() === userId.toString()) {
            userHasVoted = true;
            rating.rating = rate;
            rating.rating_date = Date.now();
        }
    }
    if(!userHasVoted) {
        this.ratings.push({
            user: userId,
            rating: rate
        });
    }
    return this;
};

/**
 * Gets the rating mean of all the songs in the album
 * @returns {number}
 */
AlbumSchema.methods.getRatingMean = function () {
    // Calcula la suma del rating de todas las canciones, y lo divide entre el numero de canciones
    return this.songs.length > 0 ? (this.songs.reduce((sumRating, song) => sumRating + song.getRatingMean(), 0) / this.songs.length) : 0;
};

/**
 * Adds a song to the album and returns the album
 * @param name Name
 * @param duration Duration in seconds
 * @param lyrics [optional] Lyrics of the song
 * @returns {AlbumSchema}
 */
AlbumSchema.methods.addSong = function (name, duration, lyrics) {
    this.songs.push({
        name: name,
        duration: duration,
        lyrics: lyrics || null,
    });
    return this;
};

/**
 * Gets the total duration of the album by doing the sum of all it's song's duration
 * @returns {number}
 */
AlbumSchema.methods.getTotalDuration = function () {
    return this.songs.length > 0 ? (this.songs.reduce((sumDuration, song) => sumDuration + song.duration, 0)) : 0;
};

/**
 * Gets a comment by its ID
 * @param commentId
 * @returns {Object}
 */
AlbumSchema.methods.getComment = function(commentId) {
    function getCommentById(id, comments) {
        let commentFound = null;
        if (comments) {
            for (let comment of comments) {
                if (comment._id.toString() === id.toString())
                    return comment;
                if (!commentFound)
                    commentFound = getCommentById(id, comment.comments);
            }
        }
        return commentFound;
    }

    return getCommentById(commentId, this.comments);
};

/**
 * Adds a comment asynchronously and returns a Promise
 * @param title Title of the comment
 * @param body Contents of the comment
 * @param userId User who posts the comment
 * @param parentId [optional] Parent comment id
 * @returns {Promise}
 */
AlbumSchema.methods.addComment = function (title, body, userId, parentId) {
    return new Promise((fulfilled, rejected) => {
        if (!userId) {
            if (rejected) rejected("User Id is required");
            else throw new Error("User Id is required");
        }

        let objectWhereToPush = this;

        if (parentId) {
            objectWhereToPush = this.getComment(parentId);
            if (!objectWhereToPush) {
                if (rejected) rejected("Not found comment by ID: " + parentId);
                else throw new Error("Not found comment by ID: " + parentId);
            }
        }

        objectWhereToPush.comments.push({
            title: title,
            body: body,
            user: userId,
        });
        fulfilled(this);
    });
};

/**
 * Gets the comments array populated asynchronously with all the user information
 * @returns {Promise}
 */
AlbumSchema.methods.getCommentsPopulated = function () {
    return new Promise(yes => {
        // Function to get recursively the userIdArray
        function getUserIdArray(array, comments) {
            let newArray = [];
            if (comments) {
                comments.forEach(comment => {
                    newArray = array.concat(getUserIdArray(array, comment.comments));
                    newArray.push(comment.user.toString());
                });
            }
            return newArray;
        }

        // Function to remove duplicates
        function unique(array) {
            let a = [];
            array.forEach(current => {
                if (a.indexOf(current) < 0) a.push(current);
            });
            return a;
        }

        // Function to populate the comments with the userMap
        function populateComments(comments, userMap) {
            if (comments) {
                comments.forEach(comment => {
                    comment.user = userMap[comment.user.toString()];
                    populateComments(comment.comments, userMap);
                });
            }
        }

        let userIdArray = unique(getUserIdArray([], this.comments));

        // We get the users from the array
        User.find({_id: {$in: userIdArray}}).then(userArray => {
            let userMap = [];
            userArray.forEach(user => {
                userMap[user._id.toString()] = user;
            });
            populateComments(this.comments, userMap);
            yes(this.comments);
        });
    });

};

module.exports = mongoose.model("Album", AlbumSchema);
