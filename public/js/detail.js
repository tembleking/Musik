// Starrr plugin (https://github.com/dobtco/starrr)
// TODO Borrar este plugin de jQuery si no se usa
const __slice = [].slice;

(function ($, window) {
    let Starrr;

    Starrr = (function () {
        Starrr.prototype.defaults = {
            rating: void 0,
            numStars: 5,
            change: function (e, value) {
            }
        };

        function Starrr($el, options) {
            let i, _ref;
            const _this = this;

            this.options = $.extend({}, this.defaults, options);
            this.$el = $el;
            _ref = this.defaults;
            for (i in _ref) {
                if (this.$el.data(i) != null) {
                    this.options[i] = this.$el.data(i);
                }
            }
            this.createStars();
            this.syncRating();
            this.$el.on('mouseover.starrr', 'span', function (e) {
                return _this.syncRating(_this.$el.find('span').index(e.currentTarget) + 1);
            });
            this.$el.on('mouseout.starrr', function () {
                return _this.syncRating();
            });
            this.$el.on('click.starrr', 'span', function (e) {
                return _this.setRating(_this.$el.find('span').index(e.currentTarget) + 1);
            });
            this.$el.on('starrr:change', this.options.change);

            // Inicializa las estrellas con el valor de attr value
            if (this.$el[0].getAttribute("value"))
                this.setRating(this.$el[0].getAttribute("value"));
            else
                console.log("Attr value needed");
        }

        Starrr.prototype.createStars = function () {
            let _i, _ref, _results;

            _results = [];

            for (_i = 1, _ref = this.options.numStars; 1 <= _ref ? _i <= _ref : _i >= _ref; 1 <= _ref ? _i++ : _i--) {
                _results.push(this.$el.append("<span class='fa fa-star-o'></span>"));
            }

            return _results;
        };

        Starrr.prototype.setRating = function (rating) {
            if (this.options.rating === rating) {
                rating = void 0;
            }
            this.options.rating = rating;
            this.syncRating();
            return this.$el.trigger('starrr:change', rating);
        };

        Starrr.prototype.syncRating = function (rating) {
            let i, _i, _j, _ref;

            rating || (rating = this.options.rating);
            if (rating) {
                for (i = _i = 0, _ref = rating - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
                    this.$el.find('span').eq(i).removeClass('fa fa-star-o').addClass('fa fa-star');
                }
            }
            if (rating && rating < 5) {
                for (i = _j = rating; rating <= 4 ? _j <= 4 : _j >= 4; i = rating <= 4 ? ++_j : --_j) {
                    this.$el.find('span').eq(i).removeClass('fa fa-star').addClass('fa fa-star-o');
                }
            }
            if (!rating) {
                return this.$el.find('span').removeClass('fa fa-star').addClass('fa fa-star-o');
            }
        };

        return Starrr;

    })();
    return $.fn.extend({
        starrr: function () {
            let args, option;

            option = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
            return this.each(function () {
                let data;

                data = $(this).data('star-rating');
                if (!data) {
                    $(this).data('star-rating', (data = new Starrr($(this), option)));
                }
                if (typeof option === 'string') {
                    return data[option].apply(data, args);
                }
            });
        }
    });
})(window.jQuery, window);

// TODO Borrar esto si no se usa
$(function () {
    return $(".starrr").starrr();
});

$(document).ready(function () {

    // Tooltip inicialization
    $('[data-toggle="tooltip"]').tooltip()


    // TODO Borrar esto si no se usa
    $('.comment-new-submit').on('click', function () {
        $.ajax({
            method: "POST",
            url: "/detail/comment",
            data: {
                album_id: $('#album-container').data("albumid"),
                comment_body: $('#comment-new-body').val(),
            }
        })
            .done(() => console.log("Success"))
            .fail(err => console.log(err));
    });

    // TODO Borrar esto si no se usa
    $('.submit-write-response').on('click', function () {
        const parent_id = $(this).data('parent-commentid');
        const body = $('#comment-response-body-' + parent_id).val();
        $.ajax({
            method: "POST",
            url: "/detail/comment",
            data: {
                album_id: $('#album-container').data("albumid"),
                comment_parent_id: $(this).data('parent-commentid'),
                comment_body: $('#comment-response-body-' + parent_id).val(),
            }
        })
            .done(() => console.log("Success"))
            .fail(err => console.log(err));

    });
});