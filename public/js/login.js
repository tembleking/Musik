$(function() {
  $('#login-form-link').click(function(e) {
	   $("#login-form").delay(100).fadeIn(100);
 		 $("#register-form").fadeOut(100);
		 $('#register-form-link').removeClass('active');
		 $(this).addClass('active');
		 e.preventDefault();
	});

  $('#register-form-link').click(function(e) {
	   $("#register-form").delay(100).fadeIn(100);
 	   $("#login-form").fadeOut(100);
		 $('#login-form-link').removeClass('active');
	   $(this).addClass('active');
	   e.preventDefault();
	});
});

$(document).ready(function(){
	$('#login-submit-guest').on('click', function(){
        $.ajax({
            method: "POST",
            url: "/login/login",
            data: {
                username: "invitado@mail.com",
                password: "invitado",
				guest: true
            }
        })
            .done(() =>   window.location = "/")
            .fail(err => console.log(err));
	});
});


