const assert = require("assert");
const User = require("../model/User");


describe("Testing User model...", function () {

    const emailForTesting = "testingEmail_827398237498@gmail.com";
    let id = "";
    let user = null;

    // We will remove the user used for testing at the end
    after(done => {
        User.findByEmail(emailForTesting).remove()
            .then(() => done())
            .catch(err => done(err));
    });


    it("Creates an User", done => {
        // We create the User object
        let newUser = User.create(emailForTesting, "password", "Name", "Surname", "1994-11-15");
        // We'll try to save it on the database
        // when the saving process has finished, we continue with the asserts
        // If there's an error, assert false and log it
        newUser.save()
            .then(newUser => {
                assert(newUser.isNew === false);
                id = newUser._id.toString();
                done();
            })
            .catch(error => {
                console.log(error);
                assert(false);
                done();
            });
    });

    it("Checks if the created user exists", done => {
        // We try to find the user on the database
        User.findByEmail(emailForTesting)
        // When the query finishes, let's check that the user found
        // is not null and the email is the same
            .then(user => {
                assert(user !== null);
                assert(user.email === emailForTesting);
                done();
            })
            .catch(err => done(err));
    });

    it("Finds user by ID", done => {
        // Let's do the same trying to find the user by it's ID
        User.findById(id)
        // When the query finishes, let's check that the user found
        // is not null and the id is the same.
        // We'll also save it on a global variable.
            .then(u => {
                assert(u._id.toString() === id.toString());
                user = u;
                done();
            })
            .catch(err => done(err));
    });

    it("Checks the password before changing it", done => {
        // We use the user saved in the global  variable, and check that
        // the password matches asynchronously.
        // When the validation finishes, we check the result.
        user.validatePassword("password")
            .then(res => {
                assert(res);
                done();
            })
            .catch(err => done(err));
    });

    it("Changes the password to 'newPassword'", done => {
        // We change the user's password asynchronously,
        // when it has finished, we save the user and
        // when the query has finished, we end the test.
        user.changePassword('newPassword')
            .then(user => user.save())
            .then(() => done())
            .catch(err => done(err));
    });

    it("Checks that the password has changed locally", done => {
        // We validate that the new password is 'newPassword'
        // asynchronously, when it finishes the check, we assert the result
        user.validatePassword('newPassword')
            .then(res => {
                assert(res);
                done();
            })
            .catch(err => done(err));
    });

    it("Checks that the user's password has changed in the database", done => {
        // We retrieve the user by it's ID from the database,
        // when the query has finished, we validate the password async,
        // and when the validation has finished, assert it and finish the test.
        User.findById(id)
            .then(user => user.validatePassword('newPassword'))
            .then(res => {
                assert(res);
                done();
            })
            .catch(err => done(err));
    });

    it("Adds actions to the user", done => {
        // Here we add multiple actions to an user, and save it.
        // When the saving process has finished, we end the test.
        user.addAction("TESTING ACTION", "This action is used for testing")
            .addAction("TESTING ACTION 2", "This is another action used for testing")
            .save()
            .then(() => done())
            .catch(err => done(err));
    });

    it("Gets actions from the user", done => {
        // We try to get the user by it's ID from the database,
        // once the query has finished, we try to assert that the user has 2 actions
        User.findById(id)
            .then(user => {
                assert(user !== null);
                assert(user.actions.length === 2);
                done();
            })
            .catch(err => done(err));
    });

});