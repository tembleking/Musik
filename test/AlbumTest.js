const assert = require("assert");
const Album = require("../model/Album");
const User = require("../model/User");


describe("Testing Album model...", function () {

    const albumNameForTesting = "Album_TESTING_23148798sda987239847";
    const genreNameForTesting = "Genre_TESTING_43958932475982379879";
    const authorNameForTesting = "Author_TESTING_8723984798798787443";
    const userEmailForTesting = "User_TESTING_2348798797987439847872@mail.com";
    let albumId = null;
    let userForTesting = null;

    before(done => {
        User.create(userEmailForTesting, "password", "TESTING", "DUMMY", "2000-01-01").save()
            .then((user) => {
                userForTesting = user;
                done();
            })
            .catch(err => done(err));
    });

    // We will remove the user used for testing at the end
    after(done => {
        Album.findByName(albumNameForTesting).remove().then(() => {
            userForTesting.remove()
                .then(() => done())
                .catch(err => done(err));
        });
    });

    it("Creates an Album", done => {
        let newAlbum = Album.create(albumNameForTesting, "Description", "2000-01-01", genreNameForTesting, authorNameForTesting);
        newAlbum.save()
            .then(album => {
                assert(album.isNew === false);
                done();
            })
            .catch(err => done(err));
    });

    it("Checks if the album Exists by Name", done => {
        Album.findByName(albumNameForTesting)
            .then(album => {
                assert(album.name === albumNameForTesting);
                assert(album.genreName === genreNameForTesting);
                assert(album.authorName === authorNameForTesting);
                albumId = album._id.toString();
                done();
            })
            .catch(err => done(err));
    });

    it("Checks if the album exists by Id", done => {
        Album.findById(albumId)
            .then(album => {
                assert(album._id.toString() === albumId);
                done();
            })
            .catch(err => done(err));
    });

    it("Checks if the album exists by Author name", done => {
        Album.findByAuthor(authorNameForTesting)
            .then(albums => {
                assert(albums.length > 0);
                let album = albums[0];
                assert(album.name === albumNameForTesting);
                assert(album.genreName === genreNameForTesting);
                assert(album.authorName === authorNameForTesting);
                done();
            })
            .catch(err => done(err));
    });

    it("Checks if the album exists by Genre name", done => {
        Album.findByGenre(genreNameForTesting)
            .then(albums => {
                assert(albums.length > 0);
                let album = albums[0];
                assert(album.name === albumNameForTesting);
                assert(album.genreName === genreNameForTesting);
                assert(album.authorName === authorNameForTesting);
                done();
            })
            .catch(err => done(err));
    });

    it("Checks if the genre exists", done => {
        Album.getGenres()
            .then(genreList => {
                assert(genreList.some(genreName => genreName === genreNameForTesting));
                done();
            })
            .catch(err => done(err));
    });

    it("Adds a song to an album", done => {
        Album.findById(albumId).then(album => {
            album
                .addSong("TestingSong1", 5000)
                .addSong("TestingSong2", 400);
            assert(album.songs.length === 2);
            album
                .save()
                .then(() => done())
                .catch(err => done(err));
        });
    });

    it("Checks if the songs has been saved", done => {
        Album.findById(albumId)
            .then(album => {
                assert(album.songs.length === 2);
                assert(album.songs[0].name === "TestingSong1");
                assert(album.songs[0].duration === 5000);
                assert(album.songs[1].name === "TestingSong2");
                assert(album.songs[1].duration === 400);
                assert(album.getTotalDuration() === 5400);
                assert(album.songs[0].getDurationString() === "01:23:20");
                done();
            })
            .catch(err => done(err));
    });

    it("Adds a rating to the song", done => {
        Album.findById(albumId)
            .then(album => {
                album.songs[0].addOrUpdateRating(userForTesting._id, 4);
                album.songs[1].addOrUpdateRating(userForTesting._id, 5);
                album.save().then(() => done());
            })
            .catch(err => done(err));
    });

    it("Checks the rating mean of the album", done => {
        Album.findById(albumId)
            .then(album => {
                assert(album.getRatingMean() === 4.5);
                done();
            })
            .catch(err => done(err));
    });

    it("Gets the user that rated the first song", done => {
        Album.findById(albumId).populate("songs.ratings.user")
            .then(album => {
                let user = album.songs[0].ratings[0].user;

                assert(user.email === userEmailForTesting);
                assert(user.name === "TESTING");
                assert(user.surname === "DUMMY");
                user.validatePassword("password").then(result => {
                    assert(result);
                    done();
                });
            })
            .catch(err => done(err));
    });

    it("Adds comments to an album", done => {
        Album.findById(albumId)
            .then(album => {
                return album.addComment("Title", "body", userForTesting._id.toString(), null)
                    .then(album => album.save())
            })
            .then(album => {
                let commentToRespond = album.comments[0];
                return album.addComment("Title2", "body2", userForTesting._id.toString(), commentToRespond._id)
                    .then(album => album.save())
            })
            .then(() => done())
            .catch(err => done(err));
    });

    it("Gets the comments populated with the user information", done => {
        Album.findById(albumId)
            .then(album => {
                album.getCommentsPopulated()
                    .then(comments => {
                        assert(comments[0].user.email === userEmailForTesting);
                        done();
                    })
                    .catch(err => done(err));
            })
            .catch(err => done(err));
    });

});