const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const config = require('../etc/config');

/**
 * Here we will connnect to the database, before testing everything
 */
before(done => {
    const dbURI = "mongodb://" + config.db.username + ":" +
        config.db.passwd + "@" +
        config.db.host + "/" +
        config.db.database;

    mongoose.connect(dbURI, {useMongoClient: true});
    mongoose.connection
        .on('connected', ref => done())
        .on("error", error => console.log(error));
});