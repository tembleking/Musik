class EventEmitter {
    constructor() {
        this.events = new Map();
    }

    /**
     * Registers an event and assigns a function to it
     * @param event Event identifier
     * @param listener Function to execute when the event is fired
     * @returns {EventEmitter}
     */
    on(event, listener) {
        if (typeof listener !== 'function') {
            throw new TypeError('The listener must be a function');
        }
        let listeners = this.events.get(event);
        if (!listeners) {
            listeners = new Set();
            this.events.set(event, listeners);
        }
        listeners.add(listener);
        return this;
    }

    /**
     * off()                    -> Removes all events
     * off(event)               -> Removes one event
     * off(event, listener)     -> Removes one listener from the event
     * @param event [Optional] Event identifier
     * @param listener [Optional] Function that's executed when the event is fired
     * @returns {EventEmitter}
     */
    off(event, listener) {
        if (!arguments.length) {
            this.events.clear();
        } else if (arguments.length === 1) {
            this.events.delete(event);
        } else {
            const listeners = this.events.get(event);
            if (listeners) {
                listeners.delete(listener);
            }
        }
        return this;
    }

    /**
     * Fires an event with the desired parameters
     * @param event Event identifier
     * @param args [Optional] Parameters the subscribed functions will receive
     * @returns {EventEmitter}
     */
    emit(event, ...args) {
        const listeners = this.events.get(event);
        if (listeners) {
            for (let listener of listeners) {
                new Promise((resolve, reject)=> {
                    listener.apply(this, args);
                });
            }
        }
        return this;
    }
}
let eventEmitter = new EventEmitter();
module.exports = eventEmitter;