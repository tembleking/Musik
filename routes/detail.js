const express = require('express');
const router = express.Router();
const Album = require("../model/Album");
const events = require("../lib/EventEmitter");

const auth = require('./ensureAuthenticated');

router.get('/:id/comments', (req, res) => {
    Album.findById(req.params.id)
        .then(album => {
            album.getCommentsPopulated()
                .then(comments => {
                    res.send(comments);
                })
                .catch(() => {
                    res.status(500);
                    res.end();
                });
        })
        .catch(() => {
            res.status(404);
            res.end();
        })
});

router.all("*", auth.ensureAuthenticated);

/* GET home page. */
router.get('/:id', function (req, res) {
    const id = req.params.id;
    Album.findById(id)
        .then(album => {
            album.getCommentsPopulated().then(comments => {

                //Modifica el objeto album para obtener la duracion de cada cancion en segundos
                let album_custom = album.toObject();
                album_custom.comments = comments;
                album_custom.release_dateString = album_custom.release_date.toISOString().substr(0, 10);

                //Se calcula la duracion de cada cancion en segundos
                album_custom.songs = album.songs.map(song => {
                    const song_tmp = song.toObject();
                    song_tmp.duration = song.getDurationString();
                    song_tmp.rating_mean = song.getRatingMean();
                    song_tmp.rating = "";

                    // If you rated the song, this will be displayed to you.
                    let own_rating = song.ratings.filter(rating => rating.user.toString() === req.user._id.toString());
                    if (own_rating.length > 0) {
                        song_tmp.rating = own_rating[0].rating;
                    }
                    return song_tmp;
                });

                res.render('detail', {
                    title: 'Web SI',
                    album: album_custom,
                    user_id: req.user._id
                });
            }).catch((err)=>console.error(err));
        });
});

router.post('/comment', (req, res) => {
    const album_id = req.body.album_id;
    const comment_parent_id = req.body.comment_parent_id || null;
    const comment_body = req.body.comment_body;
    const user_id = req.body.user_id;


    if(req.user.isGuest) {
        res.status(401);
        res.send("El usuario invitado no puede comentar");
        res.end();
        return;
    }

    Album.findById(album_id)
        .then(album => {

            // Emite los eventos correspondientes para guardar un historial de las acciones realizadas
            if(comment_parent_id){
                events.emit("USER_COMMENT_RESPONSE", req.user._id.toString(), album_id, comment_parent_id);
            }else{
                events.emit("USER_COMMENT_NEW", req.user._id.toString(), album_id);
            }

            album.addComment("NO TITLE", comment_body, user_id, comment_parent_id)
                .then(album => album.save())
                .then(() => {
                    res.status(200);
                    res.end();
                })
                .catch((err) => {
                    console.log(err);
                    res.status(500);
                    res.end();
                });
        })
        .catch((err) => {
            console.log(err);
            res.status(404);
            res.end();
        });
});

router.post('/rating', function (req, res) {
    const album_id = req.body.album_id;
    const song_name = req.body.song_name;
    const rating = req.body.rating;

    if(req.user.isGuest) {
        res.status(401);
        res.send("El usuario invitado no puede votar");
        res.end();
        return;
    }

    Album.findById(album_id)
        .then(album => {
            // console.log(album);
            let song = album.songs.filter(song => song.name === song_name)[0] || null;

            if (song) {
                song.addOrUpdateRating(req.user._id.toString(), rating);

                // Emite los eventos correspondientes para guardar un historial de las acciones realizadas
                events.emit("USER_VOTED", req.user._id.toString(), album_id, song_name, rating);
            }

            album.save().then(() => {
                res.status(200);
                res.end();
            }).catch(() => {
                res.status(500);
                res.end();
            })
        })
        .catch(() => {
            res.status(404);
            res.end();
        });

});

module.exports = router;
