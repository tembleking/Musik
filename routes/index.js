const express = require('express');
const router = express.Router();
const Album = require("../model/Album");

const auth = require('./ensureAuthenticated');

/* GET home page. */
router.get('/', function (req, res, next) {
    Album.find().then(albumsList =>{
        if(req.isAuthenticated()){
            console.log("LOGUEADO");
        }else{
            console.log("NO LOGUEADO")
        }

        res.render('index', {
            title: '',
            index_albums: albumsList

        });
    });



});

module.exports = router;
