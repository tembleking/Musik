const express = require('express');
const router = express.Router();
const Album = require("../model/Album");

const auth = require('./ensureAuthenticated');


// API
router.get("/albums", (req, res) => {
    Album.find()
        .then(albums => {
            res.send(albums);
        })
        .catch(err => console.error(err));
});

// API
router.get("/genres", (req, res) => {
    Album.getGenres()
        .then(genres => {
            res.send(genres);
        })
        .catch(err => console.error(err));
});

router.all("*", auth.ensureAuthenticated);

/* GET home page. */
router.get('/', (req, res) => {
    Album.find().then(albumsList => {
        Album.getGenres().then(genresList => {
            // Añade Todos al principio del array como genero.
            genresList.unshift("Todos");

            res.render('explore', {
                title: 'Web SI',
                genres: genresList,
                albums: albumsList,
            });
        });
    });
});

module.exports = router;
