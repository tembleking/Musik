const express = require('express');
const router = express.Router();
const User = require("../model/User");
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const auth = require('./ensureAuthenticated');
const events = require("../lib/EventEmitter");

/* GET home page. */
router.get('/', (req, res) => {
    res.render('login', {title: 'Web SI'});
});

passport.serializeUser((user, done) => {
    done(null, user._id);
});

passport.deserializeUser((id, done) => {
    User.findById(id)
        .then(user => {
            // If user's mail is a guest mail, user will have isGuest property
            if(user.email === "invitado@mail.com"){
                user = user.toObject();
                user.isGuest = true;
            }
            done(null, user)
        })
        .catch(err => done(error, null));
});

passport.use(new LocalStrategy((email, password, done) => {
        User.findByEmail(email)
            .then(user => {
                if (!user)
                    return done(null, false, {message: "Unknown email"});
                user.validatePassword(password)
                    .then((res) => {
                        if (res)
                            return done(null, user);
                        else
                            return done(null, false, {message: "Password not match"});
                    });
            });
    }
));

router.post('/login',
    passport.authenticate('local', {failureRedirect: '/login', failureFlash: true, successFlash: "Welcome"}),
    (req, res) => {

        if(req.body.guest){
            res.status(200);
            res.end();
        }else{
            if (req.body.remember)
                req.session.cookie.maxAge = 30 * 24 * 60 * 60 * 1000; // Cookie expires after 30 days
            else
                req.session.cookie.expires = false; // Cookie expires at end of session

            // Emite los eventos correspondientes para guardar un historial de las acciones realizadas
            events.emit("USER_LOGGED_IN", req.user.id.toString());
            res.redirect("/");
        }
    }
);

router.post('/register', (req, res) => {

    const email = req.body.email;
    const pass = req.body.password;

    //Validation
    req.checkBody('confirm_password', 'Passwords do not match').equals(req.body.password);
    let errors = req.validationErrors();

    if (!errors) {
        User.findByEmail(email)
            .then(user => {
                if (!user) {
                    const newUser = User.create(email, pass, req.body.name, req.body.surname, req.body.birth_date);
                    newUser.save()
                        .then(() => {
                            req.flash("success", "Registered correctly");
                            res.redirect("/login");
                        })
                        .catch(error => res.end());
                } else {
                    req.flash("error", "Email already exists");
                    res.redirect("/login");
                }
            });
    } else {
        req.flash("error", "Passwords do not match");
        res.redirect("/login");
    }
});

// Todas las demas rutas.
//------------------------------------------------------------------------------
router.all("*", auth.ensureAuthenticated);
//------------------------------------------------------------------------------

//LOGOUT
router.get('/logout', (req, res) => {
    req.logout();
    req.flash("success", "Logged out correctly, bye");
    res.clearCookie('remember_user');
    res.redirect("/login");
});

module.exports = router;
