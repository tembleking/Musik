const express = require('express');
const router = express.Router();

const auth = require('./ensureAuthenticated');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('privacy', { title: 'Web SI' });
});

module.exports = router;
