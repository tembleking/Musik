const express = require('express');
const router = express.Router();
const User = require('../model/User');

const auth = require('./ensureAuthenticated');
router.all("*", auth.ensureAuthenticated);

/* GET home page. */
router.get('/', function (req, res) {
    User.findById(req.user._id.toString()).then(user => {

        // Cambia el formato de la fecha de las acciones
        const user_tmp = user.toObject();
        user_tmp.actions = user.actions.map(action => {
            const action_tmp = action.toObject();
            action_tmp.date = action.date.toISOString().substr(0, 10);
            return action_tmp;
        });

        res.render('settings', {
            title: 'Web SI',
            actions: user_tmp.actions
        });
    }).catch((err) => {
        console.log(err)
        res.status(404);
        res.end();
    });

});

module.exports = router;
